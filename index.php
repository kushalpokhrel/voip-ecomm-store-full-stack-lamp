<?php
session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="shortcut icon" href="img/yealink/Yealink-MP-56-for-Microsoft-Teams5_0x220.jpg" />
        <title>VoIP Partners Store</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- latest compiled and minified CSS -->
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" type="text/css">
        <!-- jquery library -->
        <script type="text/javascript" src="bootstrap/js/jquery-3.2.1.min.js"></script>
        <!-- Latest compiled and minified javascript -->
        <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
        <!-- External CSS -->
        <link rel="stylesheet" href="css/style.css" type="text/css">
    </head>
    <body>
        <div>
           <?php
            require 'header.php';
           ?>
           <div id="bannerImage">
               <div class="container">
                   <center>
                   <div id="bannerContent">
                       <h1>We sell all kinds of VoIP Products.</h1>
                       <p>Flat 40% OFF on all premium brands.</p>
                       <a href="https://voippartners.com.au/#get-started" class="btn btn-danger">Shop Now</a>
                   </div>
                   </center>
               </div>
           </div>
           <div class="container">
               <div class="row">
                   <div class="col-xs-4">
                       <div  class="thumbnail">
                           <a href="products.php">
                                <img src="img/yealink/Yealink-MP-56-for-Microsoft-Teams5_0x220.jpg" alt="Camera">
                           </a>
                           <center>
                                <div class="caption">
                                        <p id="autoResize">Cloud PBX Services</p>
                                        <p>Choose among the best Cloud PBX Systems.</p>
                                </div>
                           </center>
                       </div>
                   </div>
                   <div class="col-xs-4">
                       <div class="thumbnail">
                           <a href="products.php">
                               <img src="img/yealink/Yealink-MP-56-for-Microsoft-Teams5_0x220.jpg" alt="Watch">
                           </a>
                           <center>
                                <div class="caption">
                                    <p id="autoResize">Sip Phones</p>
                                    <p>Yealink SIP Enabled phones at best rates</p>
                                </div>
                           </center>
                       </div>
                   </div>
                   <div class="col-xs-4">
                       <div class="thumbnail">
                           <a href="products.php">
                               <img src="img/yealink/Yealink-MP-56-for-Microsoft-Teams5_0x220.jpg" alt="Shirt">
                           </a>
                           <center>
                               <div class="caption">
                                   <p id="autoResize">NBN Internet Plans</p>
                                   <p>Our exquisite collection of NBN Plans for 25/50/100 mbps</p>
                               </div>
                           </center>
                       </div>
                   </div>
               </div>
           </div>
            <br><br> <br><br><br><br>
           <footer class="footer"> 
               <div class="container">
               <center>
                   <p>Copyright &copy <a href="https://Kushalpokhrel.w3spaces.com">Kushal Pokhrel</a> All Rights Reserved.</p>
                   <p>This website is developed by Kushal Pokhrel</p>
               </center>
               </div>
           </footer>
        </div>
    </body>
</html>